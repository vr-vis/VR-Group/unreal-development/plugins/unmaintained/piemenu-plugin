// Fill out your copyright notice in the Description page of Project Settings.


#include "Slices/CheckBoxSlice.h"
#include "EventHandlerActor.h"

// Sets default values for this component's 
UCheckBoxSlice::UCheckBoxSlice()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	// PrimaryComponentTick.bCanEverTick = true;

	Width = 22.f;
	Height = 10.f;

	this->SliceType = EMenuSliceTypes::TYPE_CheckBox;

	//TODO : Will be changed
	static ConstructorHelpers::FClassFinder<UUserWidget> WidgetHolder(TEXT("/RadialPieMenu/SliceEquipments/CheckBoxWidget"));

	if (WidgetHolder.Succeeded())
	{
		WidgetClass = WidgetHolder.Class;
	}
	else
	{
		WidgetClass = nullptr;
	}
}

// Called when the game 
void UCheckBoxSlice::BeginPlay()
{
	Super::BeginPlay();

	UserWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);

	this->SetWidget(UserWidget);
	this->SetDrawSize(FVector2D(Width, Height));

	SetWidgetPosition(this, sliceOffset, outerRadius, innerRadius, depth, Width, Height, degree);
	CheckBox = (UCheckBox*)UserWidget->WidgetTree->FindWidget(FName("CheckBox"));

	if (CheckBox)
	{
		CheckBox->OnCheckStateChanged.AddDynamic(this, &UCheckBoxSlice::OnCheckStateChanged);
	}
	changeWidgetVisibility();
}

// Called every frame
void UCheckBoxSlice::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UCheckBoxSlice::OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed)
{
	Super::OnClicked(Component, ButtonPressed);

	if (OldCheckBoxState == CheckBoxState)
	{
		CheckBoxState = !CheckBoxState;
		CheckBox->SetCheckedState(CheckBoxState ? ECheckBoxState::Checked : ECheckBoxState::Unchecked);
	}

	OldCheckBoxState = CheckBoxState;

	//FString State = CheckBoxState ? "True" : "False";
	//GEngine->AddOnScreenDebugMessage(1, 2.f, FColor::Red, "CheckBox State Changed To " + State);
}

bool UCheckBoxSlice::GetCheckBoxState()
{
	return CheckBoxState;
}

void UCheckBoxSlice::OnCheckStateChanged(bool InState)
{
	CheckBoxState = InState;
	this->OnClicked(this, EKeys::LeftMouseButton.GetFName());

	//Send state changed event from here
}
