// Fill out your copyright notice in the Description page of Project Settings.

#include "Slices/BackSlice.h"
#include "EventHandlerActor.h"
#include "Menu.h"

// Sets default values for this component's 
UBackSlice::UBackSlice()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame. You can turn these features
	// off to improve performance if you don't need them.
	// PrimaryComponentTick.bCanEverTick = true;

	//These determine the size of the back button
	Width = 15.f;
	Height = 15.f;

	this->SliceType = EMenuSliceTypes::TYPE_Back;

	//TODO : Will be changed
	static ConstructorHelpers::FClassFinder<UUserWidget> WidgetHolder(TEXT("/RadialPieMenu/SliceEquipments/BackWidget"));
	if (WidgetHolder.Succeeded())
	{
		WidgetClass = WidgetHolder.Class;
	}
	else
	{
		WidgetClass = nullptr;
	}
}

// Called when the game starts
void UBackSlice::BeginPlay()
{
	Super::BeginPlay();

	UserWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);

	this->SetWidget(UserWidget);
	this->SetDrawSize(FVector2D(Width, Height));

	BackButton = (UButton*)UserWidget->WidgetTree->FindWidget(FName("Back"));

	if (BackButton)
	{
		BackButton->OnPressed.AddDynamic(this, &UBackSlice::OnBackButtonPressed);
	}
}

// Called every frame
void UBackSlice::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UBackSlice::OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed)
{
	Super::OnClicked(Component, ButtonPressed);
}

void UBackSlice::OnBackButtonPressed()
{
	this->OnClicked(this, EKeys::LeftMouseButton.GetFName());
}
