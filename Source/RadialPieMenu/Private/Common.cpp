// Fill out your copyright notice in the Description page of Project Settings.

#include "Common.h"

void SetWidgetPosition(UWidgetComponent *WidgetComponent, float StartAlpha, float OuterRadius, float InnerRadius, float Depth, float Width, float Height, int PieDegree)
{
	float Theta = StartAlpha + PieDegree / 2.f;
	float Radius = (OuterRadius + InnerRadius) / 2.f;

	float XValue = Depth + 1.f;
	float YValue = -Radius * FMath::Cos(Theta * PI / 180);
	float ZValue = Radius * FMath::Sin(Theta * PI / 180);

	FVector Location = FVector(XValue, YValue, ZValue);
	FRotator Rotation = FRotator(0.f, 0.f, StartAlpha + PieDegree / 2.f);

	WidgetComponent->SetRelativeLocationAndRotation(Location, Rotation, false, nullptr, ETeleportType::None);
}
