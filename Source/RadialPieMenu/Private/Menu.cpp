// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu.h"
#include "Common.h"

#include "CheckBoxSlice.h"
#include "SliderSlice.h"
#include "ColorPickerSlice.h"
#include "RadioButtonSlice.h"


// Sets default values for this component's properties
UMenu::UMenu()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	FStringAssetReference MaterialPath(TEXT("/RadialPieMenu/Materials/RadialMaterial"));
	material = (UMaterial*) MaterialPath.TryLoad();
	// ...
}

// Called when the game starts
void UMenu::BeginPlay()
{
	Super::BeginPlay();

	//needed for multiple plays in the editor
	currentMenuLevel = 0;

	//only call this for the main menu
	if (this == GetAttachmentRoot())
	{
		mainMenu = this;
		CreateMenuStructure();
		GenerateMenu();
	}
	else
	{
		this->CloseMenu();
	}

}

// Called every frame
void UMenu::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Sets a fixed position in the world
	static int levelDist = 250;

	//location = FVector(500.0f, 0.0f, 0.0f);
	location = FVector(0.0f, 0.0f, 100.0f) - (FVector(1.0f, 0.0f, 0.0f) * (levelDist * menuLevel * 0.5));
	rotation = FRotator(0.0f, 0.0f, 180.0f);

	this->SetWorldLocationAndRotation(location, rotation);
}

//Sets submenu slices to visible
void UMenu::OpenMenu()
{
		for (USlice* slice : pieSlices)
		{
			slice->ProceduralMesh->SetVisibility(true);
		}
}

//Sets visibility of parent menu slices to false
void UMenu::CloseMenu()
{
		for (USlice* slice : pieSlices)
		{
			slice->ProceduralMesh->SetVisibility(false);
			//slice->scaleSliceOpacityAndColor(1.0f);
		}
}

void UMenu::CreateMenuStructure()
{
	FString name = this->GetName();
	
	if (parentMenu)
	{
		//we got ourselves a submenu here, so add a BackSlice
		backSlice = NewObject<UBackSlice>(this, FName("BackSlice"));
		backSlice->sliceLevel = this->menuLevel;
		backSlice->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);
		backSlice->RegisterComponent();
	}

	TArray<USceneComponent*> childComponents;
	GetChildrenComponents(false, childComponents);	//only get direct children

	for (USceneComponent* component : childComponents)
	{
		USlice* slice = (USlice*)component;
		pieSlices.Add(slice);
		slice->sliceLevel = this->menuLevel;
		TArray<USceneComponent*> subMenuSlices;
		slice->GetChildrenComponents(false, subMenuSlices);

		//can be either 0 or 1 child
		if (subMenuSlices.Num() > 0)
		{
			UMenu* subMenu = (UMenu*)subMenuSlices[0];
			subMenus.Add(slice, subMenu);

			subMenu->parentMenu = this;
			subMenu->menuLevel = this->menuLevel + 1;
			subMenu->CreateMenuStructure();
		}
	}
}


void UMenu::GenerateMenu()
{
	int numberOfSlices = pieSlices.Num();

	float segmentOffset = 0.0f;
	int totalPercentage = 0;

	for (int place = 0; place < numberOfSlices; ++place)
	{
		if (pieSlices[place]->SliceType != EMenuSliceTypes::TYPE_Back)
		{
			totalPercentage += pieSlices[place]->Percentage;
		}
	}

	int numSlices = numberOfSlices;
	
	if (this != mainMenu)
		numSlices--;	//ignore backslice

	float gapBetweenSlices = ((100 - totalPercentage) / (numSlices)) * 3.6f;

	for (int place = 0; place < numberOfSlices; place++)
	{
		pieSlices[place]->GenerateSliceMesh(place, segmentOffset, material);

		if (pieSlices[place]->SliceType != EMenuSliceTypes::TYPE_Back)
		{
			segmentOffset += FMath::FloorToInt(360 * pieSlices[place]->Percentage / 100) + gapBetweenSlices;
		}
	}

	//recursively generate all submenus
	for (const TPair<USlice*, UMenu*>& submenu : subMenus)
	{
		submenu.Value->GenerateMenu();
	}
}

UMenu* UMenu::findParentMenuOfSlice(USlice* slice)
{
	TQueue<UMenu*> menuQueue;
	menuQueue.Enqueue(mainMenu);

	while (!menuQueue.IsEmpty())
	{
		UMenu* CurrentMenu;
		menuQueue.Dequeue(CurrentMenu);

		if (CurrentMenu->pieSlices.Contains(slice))
		{
			return CurrentMenu;
		}

		//add all submenus to the queue
		for (TPair<USlice*, UMenu*> submenu : CurrentMenu->subMenus)
		{
			menuQueue.Enqueue(submenu.Value);
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("couldn't find parent menu of the slice"));
	return nullptr;
}

//Sets colliosion, such that only the newly opened menu can be interacted with
void UMenu::SetMenuCollision()
{
	TQueue<UMenu*> menuQueue;
	menuQueue.Enqueue(mainMenu);

	while (!menuQueue.IsEmpty())
	{
		UMenu* currentMenu;
		menuQueue.Dequeue(currentMenu);

		for (USlice* slice : pieSlices)
		{
			slice->SetCollision();
		}

		for (TPair<USlice*, UMenu*> submenu : currentMenu->subMenus)
		{
			menuQueue.Enqueue(submenu.Value);
		}
	}
}

TMap<FString, FVariant> UMenu::GetAllValues()
{
	TMap<FString, FVariant> Values;

	TQueue<UMenu*> MenuQueue;
	MenuQueue.Enqueue(mainMenu);

	while(!MenuQueue.IsEmpty())
	{
		UMenu* CurrentMenu;
		MenuQueue.Dequeue(CurrentMenu);

		for(USlice* Slice : CurrentMenu->pieSlices)
		{
			if(Slice->SliceType == EMenuSliceTypes::TYPE_CheckBox)
			{
				UCheckBoxSlice* CheckBoxSlice = Cast<UCheckBoxSlice>(Slice);
				Values.Add(CheckBoxSlice->GetName(), CheckBoxSlice->GetCheckBoxState());
			}
			else if (Slice->SliceType == EMenuSliceTypes::TYPE_ColorPicker)
			{
				UColorPickerSlice* ColorPickerSlice = Cast<UColorPickerSlice>(Slice);
				Values.Add(ColorPickerSlice->GetName(), ColorPickerSlice->GetColor());
			}
			else if(Slice->SliceType == EMenuSliceTypes::TYPE_Slider)
			{
				USliderSlice* SliderSlice = Cast<USliderSlice>(Slice);
				Values.Add(SliderSlice->GetName(), SliderSlice->GetSliderValue());
			}
			else if(Slice->SliceType == EMenuSliceTypes::TYPE_RadioButton)
			{
				URadioButtonSlice* RadioButtonSlice = Cast<URadioButtonSlice>(Slice);
				Values.Add(RadioButtonSlice->GetName(), RadioButtonSlice->GetRadioButtonState());
			}
		}

		for(TPair<USlice*, UMenu*> Submenu : CurrentMenu->subMenus)
		{
			MenuQueue.Enqueue(Submenu.Value);
		}
	}
	
	return Values;
}
