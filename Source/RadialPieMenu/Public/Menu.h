// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Materials/Material.h"
#include "Slice.h"
#include "BackSlice.h"
#include "EventHandlerActor.h"
#include "Misc/Variant.h"

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Menu.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RADIALPIEMENU_API UMenu : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMenu();
	void OpenMenu();
	void CloseMenu();
	void SetMenuCollision();
	
	TArray<USlice*> GetPieSlices() const { return pieSlices; }

	/**
	 * Returns a TMap containing all currently set values of all existing menus/submenus
	 */
	static TMap<FString, FVariant> GetAllValues();
	
	UMenu* findParentMenuOfSlice(USlice* slice);
	UPROPERTY() UMaterial* material;	//prevents garbage collection
	UPROPERTY() TMap<USlice*, UMenu*> subMenus;
	UPROPERTY() UBackSlice* backSlice;

	int menuLevel = 0;
	
	
protected:
	// Called when the game starts
	UMenu* parentMenu;

	FRotator rotation;
	FVector direction;
	FVector location;
	

	// Called every frame
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	

private:
	//creates the menu structure, duh
	void CreateMenuStructure();

	TArray<USlice*> pieSlices;

	//creates the visible representation (mesh) of this menu
	virtual void GenerateMenu();
};
