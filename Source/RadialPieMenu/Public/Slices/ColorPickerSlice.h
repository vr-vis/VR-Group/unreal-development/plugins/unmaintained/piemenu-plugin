// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Slice.h"
#include "Components/Button.h"
#include "Components/CanvasPanel.h"

#include "ColorPickerSlice.generated.h"

/**
 * TODO: Change this class to use Slate for color picking
 */
UCLASS(ClassGroup = (Components), meta = (BlueprintSpawnableComponent))
class RADIALPIEMENU_API UColorPickerSlice : public USlice
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UColorPickerSlice();

	FLinearColor GetColor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed) override;

	UButton *ColorMap;
	UCanvasPanel *CanvasPanel;

	FLinearColor ChosenColor;

	UFUNCTION()
	void OnMouseClicked();
};