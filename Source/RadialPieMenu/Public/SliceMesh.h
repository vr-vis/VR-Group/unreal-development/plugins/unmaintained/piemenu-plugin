// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ProceduralMeshComponent.h"
#include "Materials/Material.h"

class USlice;

/**
 * Generates the mesh of a slice. Uses static methods to do so
 */
class RADIALPIEMENU_API SliceMesh
{
public:
	SliceMesh();
	~SliceMesh();
	static void GenerateSliceMesh(USlice* slice, int place, float segmentOffset, UMaterial* material);

private:
	static void GenerateMesh(USlice* slice, float BegAlpha, float EndAlpha, bool Bottom, bool Top);
	static void AddQuadMesh(USlice* slice, FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32& QuadIndex, FProcMeshTangent Tangent);

};
